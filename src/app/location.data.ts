export type LocationData = {
    customTitle: string;
	street: string;
	plz: string;
	city: string;
	state: string;
	country: string;
};