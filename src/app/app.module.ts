import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { LocationModal } from './LocationModalComponent/locationModal.component';
import { LocationSearch } from './LocationSearchComponent/locationSearch.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { ScrollingModule } from '@angular/cdk/scrolling';
import { MatButtonModule } from '@angular/material/button';

import { HttpClientModule } from '@angular/common/http';

@NgModule({
  declarations: [
    LocationModal,
	LocationSearch
  ],
  imports: [
	HttpClientModule,
    BrowserModule,
    BrowserAnimationsModule,
	MatButtonModule,
	ScrollingModule
  ],
  providers: [],
  bootstrap: [LocationModal]
})
export class AppModule { }
