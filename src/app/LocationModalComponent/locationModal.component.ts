import { HttpClient } from '@angular/common/http';
import { Component } from '@angular/core';
import { LocationService } from 'src/app/location.service';
import { LocationData } from 'src/app/location.data';

@Component({
  selector: 'location-modal',
  templateUrl: './locationModal.component.html',
  styleUrls: ['./locationModal.component.sass']
})
export class LocationModal {
  showSearch = false;
  results: LocationData[] = []

  constructor(private locationService: LocationService) {}

  closeModal() {
	  this.showSearch = false;
  }
  onSubmitLocation(value: LocationData) {
	  this.results.push(value);
  }
  onSearchClick() {
	  this.showSearch = true;
  }
}
