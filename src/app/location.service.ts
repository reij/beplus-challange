import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { LocationData } from 'src/app/location.data';

@Injectable({
  providedIn: 'root'
})
export class LocationService {
	readonly photonQueryUrl: string = "https://photon.komoot.io/api/?q=";

	constructor(private http: HttpClient) { }
	
	GetLocationResultsFromQuery(query: string): Promise<LocationData[]> {
		return new Promise(async (resolve) => {
			const queryUrl = `${this.photonQueryUrl}${query}`;
			const results: LocationData[] = [];
			this.http.get(queryUrl).subscribe(result => {
				const resultJson = JSON.parse(JSON.stringify(result));
				for (var feature of resultJson["features"]) {
					var properties = feature["properties"];
					var street = properties['name'];
					if (street) {
						var location = {
							customTitle: '',
							street: properties['name'],
							plz: properties['postcode'],
							city: properties['city'],
							state: properties['state'],
							country: properties['country']
						} as LocationData;
						results.push(location);
					}
				}
				resolve(results);
			});
		});
	}
}
