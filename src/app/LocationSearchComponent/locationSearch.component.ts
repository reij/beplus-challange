import { Output } from '@angular/core';
import { Component, EventEmitter } from '@angular/core';
import { LocationService } from 'src/app/location.service';
import { LocationData } from 'src/app/location.data';

@Component({
  selector: 'location-search',
  templateUrl: './locationSearch.component.html',
  styleUrls: ['./locationSearch.component.sass']
})
export class LocationSearch {
  @Output()
  onClose: EventEmitter<void> = new EventEmitter();

  @Output()
  onSubmitLocation: EventEmitter<LocationData> = new EventEmitter();

  constructor(private locationService: LocationService) {}

  selectedLocation = "";
  customTitle = "";
  searchResults: LocationData[] = [];
  selectedResult!: LocationData;

  OnCustomTitleInput(event: any) {
	  this.customTitle = event.target.value;
  }
  async OnTextSearch(event: any) {
	const value = event.target.value;
	const result = await this.locationService.GetLocationResultsFromQuery(value);
	if(result.length > 0) {
		this.searchResults = result;
	}
  }
  OnItemClick(item: LocationData) {
	this.selectedResult = item;
	this.searchResults = [];
	this.selectedLocation = `${item.street} ${item.city}`;
  }
  OnCloseClick() {
	  this.onClose.emit();
  }
  OnSaveClick() {
	  this.selectedResult.customTitle = this.customTitle;
	  this.onSubmitLocation.emit(this.selectedResult);
	  this.onClose.emit();
  }
}
